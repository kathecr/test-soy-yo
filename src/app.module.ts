import { Module } from '@nestjs/common';
import { ENTITIES_REPOSITORY } from './common/constans';
import { ConfigModule } from '@nestjs/config';

import { AppController } from './controllers/app.controller';
import { ApiEntitiesRepository } from './repositories/api/api-entities.repository';
import { AppService } from './services/app.service';
import configuration from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env', load: [configuration] }),
  ],
  controllers: [AppController],
  providers: [
    {
      provide: ENTITIES_REPOSITORY,
      useClass: ApiEntitiesRepository,
    },
    AppService,
  ],
})
export class AppModule {}
