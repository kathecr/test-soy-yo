export const ENTITIES_REPOSITORY = Symbol('ENTITIES_REPOSITORY');

export const MESSAGE_NOT_FOUND = 'Data not found';

export const MESSAGE_ERROR_RESULT = 'Error en validación datos de entrada';
