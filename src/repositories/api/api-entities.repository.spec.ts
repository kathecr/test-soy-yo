import { Test, TestingModule } from '@nestjs/testing';
import { ApiEntitiesRepository } from './api-entities.repository';

describe('ApiEntitiesRepository', () => {
  let repository: ApiEntitiesRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiEntitiesRepository],
    }).compile();

    repository = module.get<ApiEntitiesRepository>(ApiEntitiesRepository);
  });

  it('should be defined', () => {
    expect(repository).toBeDefined();
  });
});
