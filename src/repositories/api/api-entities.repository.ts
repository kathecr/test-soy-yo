import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

import {
  EntitiesRepository,
  GetEntityResultModel,
} from '../entities.repository';

@Injectable()
export class ApiEntitiesRepository implements EntitiesRepository {
  private _apiUrl: string;
  constructor(private configService: ConfigService) {
    this._apiUrl = this.configService.get('GET_ENTITY_URL');
  }

  async getEntity(id: number): Promise<GetEntityResultModel> {
    const { data } = await axios.get<GetEntityResultModel>(
      `${this._apiUrl}/${id}`,
    );
    return data;
  }
}
