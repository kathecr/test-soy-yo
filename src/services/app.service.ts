import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';

import {
  ENTITIES_REPOSITORY,
  MESSAGE_ERROR_RESULT,
  MESSAGE_NOT_FOUND,
} from '../common/constans';
import {
  FilterEntitiesItemResponseModel,
  FilterEntitiesRequestModel,
  FilterEntitiesResponseModel,
} from '../models/app.model';
import {
  EntitiesRepository,
  GetEntityResultModel,
} from '../repositories/entities.repository';

@Injectable()
export class AppService {
  constructor(
    @Inject(ENTITIES_REPOSITORY) private entitiesRepository: EntitiesRepository,
  ) {}

  async listEntitiesByFilters({
    startId,
    endId,
  }: FilterEntitiesRequestModel): Promise<FilterEntitiesResponseModel> {
    const promises: Promise<GetEntityResultModel>[] = [];

    for (let index = startId; index <= endId; index++) {
      promises.push(this.entitiesRepository.getEntity(index));
    }
    let results: GetEntityResultModel[] = [];
    try {
      results = await Promise.all(promises);
    } catch (error) {
      throw new HttpException(
        { Error: MESSAGE_ERROR_RESULT },
        HttpStatus.NOT_FOUND,
      );
    }

    const foundError = results.find(
      (item) => item.message === MESSAGE_NOT_FOUND,
    );

    if (foundError) {
      throw new HttpException(
        { Error: MESSAGE_ERROR_RESULT },
        HttpStatus.NOT_FOUND,
      );
    }

    return {
      items: results.map<FilterEntitiesItemResponseModel>((item) => ({
        contactEmail: item.data.contactMail,
        expirationDate: item.data.expirationDate,
        contactName: item.data.contactName,
        identificationNumber: item.data.identificationNumber,
        logo: item.data.logo,
        name: item.data.name,
        ...{
          ...(item.data.entityId ? { entityId: item.data.entityId } : null),
        },
      })),
    };
  }
}
