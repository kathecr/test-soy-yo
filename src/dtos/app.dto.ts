import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { IsBiggerThan } from '../common/pipes/is-bigger-than.pipe';

import {
  FilterEntitiesItemResponseModel,
  FilterEntitiesRequestModel,
  FilterEntitiesResponseModel,
} from '../models/app.model';

export class ListEntitiesRequestDto implements FilterEntitiesRequestModel {
  @IsNumber()
  @ApiProperty({
    type: 'number',
    description: 'start id',
  })
  startId: number;

  @IsNumber()
  @IsBiggerThan('startId', {
    message: 'endId must be larger than startId',
  })
  @ApiProperty({
    type: 'number',
    description: 'end id',
  })
  endId: number;
}

export class ListEntitiesResponseDto implements FilterEntitiesResponseModel {
  @ApiProperty({
    type: 'array',
    items: {
      type: 'object',
      additionalProperties: false,
      required: [],
      properties: {
        entityId: { type: 'number' },
        name: { type: 'string' },
        identificationNumber: { type: 'string' },
        expirationDate: { type: 'string' },
        contactName: { type: 'string' },
        contactEmail: { type: 'string' },
        logo: { type: 'string' },
      },
    },
  })
  items: FilterEntitiesItemResponseModel[];
}
