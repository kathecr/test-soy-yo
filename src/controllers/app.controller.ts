import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import {
  ListEntitiesRequestDto,
  ListEntitiesResponseDto,
} from '../dtos/app.dto';
import { AppService } from '../services/app.service';

@ApiTags('Entities')
@Controller('entities')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('filter')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Success response',
    type: ListEntitiesResponseDto,
  })
  filterPayload(
    @Body() payload: ListEntitiesRequestDto,
  ): Promise<ListEntitiesResponseDto> {
    return this.appService.listEntitiesByFilters(payload);
  }
}
